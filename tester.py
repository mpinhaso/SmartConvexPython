from osgeo import gdal, osr, ogr
import numpy as np
import os
import csv
import subprocess
import math



def import_summary_geojson(geojsonfilename):
    # driver = ogr.GetDriverByName('geojson')
    datasource = ogr.Open(geojsonfilename, 0)

    layer = datasource.GetLayer()
    print(layer.GetFeatureCount())

    buildingList = []
    for idx, feature in enumerate(layer):

        poly = feature.GetGeometryRef()

        if poly:
            buildingList.append({'poly': feature.GetGeometryRef().Clone()})

    return buildingList

def getSmartConvex(polygon, areaSize):
    if not polygon.GetGeometryType() == 3:
        newpoly = ogr.Geometry(ogr.wkbPolygon)
        newpoly.AddGeometry(polygon)
        polygon = newpoly
    # Calculate convex hull
    convexhull = polygon.ConvexHull()
    difference = convexhull.Difference(polygon)
    for geom_part in difference:
        if geom_part.GetArea() > areaSize:
            convexhull = convexhull.Difference(getSmartConvex(geom_part, areaSize))
    return convexhull

def exportConvexHull(geojsonfilename, areaSize = None):

    datasource = ogr.Open(geojsonfilename, 0)

    layer = datasource.GetLayer()
    print(layer.GetFeatureCount())
    if areaSize == None:
        arr = []
        for ids, feature in enumerate(layer):
            poly = feature.GetGeometryRef()
            area = poly.GetArea()
            arr.append(area)
        areaSize = np.mean(arr) / 20

    convexList = []
    for idx, feature in enumerate(layer):
        poly = feature.GetGeometryRef()
        polyConvexed = getSmartConvex(poly, areaSize)
        convexList.append({'id': idx, 'poly': polyConvexed})

    # Save extent to a new Shapefile
    file_Name = os.path.basename(geojsonfilename)
    fileName = os.path.splitext(file_Name)[0]
    dirName = os.path.dirname(geojsonfilename)

    outFile = os.path.join(dirName, fileName + '_simplify.geojson')
    print outFile
    outDriver = ogr.GetDriverByName("GeoJSON")

    # Remove output shapefile if it already exists
    if os.path.exists(outFile):
        outDriver.DeleteDataSource(outFile)

    # Create the output shapefile
    outDataSource = outDriver.CreateDataSource(outFile)
    outLayer = outDataSource.CreateLayer("buildings_convexhull", geom_type=ogr.wkbPolygon)
    # Add an ID field
    idField = ogr.FieldDefn("id", ogr.OFTInteger)
    outLayer.CreateField(idField)
    featureDefn = outLayer.GetLayerDefn()

    for convex in convexList:
        # Create the feature and set values
        feature = ogr.Feature(featureDefn)
        feature.SetGeometry(convex["poly"])
        feature.SetField("id", convex["id"])
        outLayer.CreateFeature(feature)
        feature = None

    # Save and close DataSource
    inDataSource = None
    outDataSource = None

#exportConvexHull("/home/meni/Downloads/goData/3band_013022223130_Public_img37_pred.geojson")
exportConvexHull("/home/menny/Documents/projects/python/gdal/smartConvex/files/1poly.geojson", 5)
print("yes")